" Preamble {{{
" ==============================================================================
"        File: russian-mnemonic.vim
" ------------------------------------------------------------------------------
"     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
" Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"     License: This program is free software: you can redistribute it and/or
"              modify it under the terms of the GNU General Public License as
"              published by the Free Software Foundation, either version 3 of
"              the License, or (at your option) any later version.
"
"              This program is distributed in the hope that it will be useful,
"              but WITHOUT ANY WARRANTY; without even the implied warranty of
"              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
"              General Public License for more details.
"
"              You should have received a copy of the GNU General Public License
"              along with this program. If not, see
"              <http://www.gnu.org/licenses/>.
" ==============================================================================
" }}} Preamble

scriptencoding utf-8

let b:keymap_name = 'ru'

loadkeymap
A  А CYRILLIC CAPITAL LETTER A
B  Б CYRILLIC CAPITAL LETTER BE
V  В CYRILLIC CAPITAL LETTER VE
G  Г CYRILLIC CAPITAL LETTER GHE
D  Д CYRILLIC CAPITAL LETTER DE
E  Е CYRILLIC CAPITAL LETTER IE
Jo Ё CYRILLIC CAPITAL LETTER IO
Yo Ё CYRILLIC CAPITAL LETTER IO
JO Ё CYRILLIC CAPITAL LETTER IO
YO Ё CYRILLIC CAPITAL LETTER IO
Zh Ж CYRILLIC CAPITAL LETTER ZHE
ZH Ж CYRILLIC CAPITAL LETTER ZHE
Z  З CYRILLIC CAPITAL LETTER ZE
I  И CYRILLIC CAPITAL LETTER I
J  Й CYRILLIC CAPITAL LETTER SHORT I
K  К CYRILLIC CAPITAL LETTER KA
L  Л CYRILLIC CAPITAL LETTER EL
M  М CYRILLIC CAPITAL LETTER EM
N  Н CYRILLIC CAPITAL LETTER EN
O  О CYRILLIC CAPITAL LETTER O
P  П CYRILLIC CAPITAL LETTER PE
R  Р CYRILLIC CAPITAL LETTER ER
S  С CYRILLIC CAPITAL LETTER ES
T  Т CYRILLIC CAPITAL LETTER TE
U  У CYRILLIC CAPITAL LETTER U
F  Ф CYRILLIC CAPITAL LETTER EF
H  Х CYRILLIC CAPITAL LETTER HA
C  Ц CYRILLIC CAPITAL LETTER TSE
Ch Ч CYRILLIC CAPITAL LETTER CHE
CH Ч CYRILLIC CAPITAL LETTER CHE
Sh Ш CYRILLIC CAPITAL LETTER SHA
SH Ш CYRILLIC CAPITAL LETTER SHA
Sc Щ CYRILLIC CAPITAL LETTER SHCHA
SC Щ CYRILLIC CAPITAL LETTER SHCHA
Q  Ъ CYRILLIC CAPITAL LETTER HARD SIGN
Y  Ы CYRILLIC CAPITAL LETTER YERU
W  Ь CYRILLIC CAPITAL LETTER SOFT SIGN
Je Э CYRILLIC CAPITAL LETTER E
Ye Э CYRILLIC CAPITAL LETTER E
JE Э CYRILLIC CAPITAL LETTER E
YE Э CYRILLIC CAPITAL LETTER E
Ju Ю CYRILLIC CAPITAL LETTER YU
Yu Ю CYRILLIC CAPITAL LETTER YU
JU Ю CYRILLIC CAPITAL LETTER YU
YU Ю CYRILLIC CAPITAL LETTER YU
Ja Я CYRILLIC CAPITAL LETTER YA
Ya Я CYRILLIC CAPITAL LETTER YA
JA Я CYRILLIC CAPITAL LETTER YA
YA Я CYRILLIC CAPITAL LETTER YA

a  а CYRILLIC SMALL LETTER A
b  б CYRILLIC SMALL LETTER BE
v  в CYRILLIC SMALL LETTER VE
g  г CYRILLIC SMALL LETTER GHE
d  д CYRILLIC SMALL LETTER DE
e  е CYRILLIC SMALL LETTER IE
jo ё CYRILLIC SMALL LETTER IO
yo ё CYRILLIC SMALL LETTER IO
zh ж CYRILLIC SMALL LETTER ZHE
z  з CYRILLIC SMALL LETTER ZE
i  и CYRILLIC SMALL LETTER I
j  й CYRILLIC SMALL LETTER SHORT I
k  к CYRILLIC SMALL LETTER KA
l  л CYRILLIC SMALL LETTER EL
m  м CYRILLIC SMALL LETTER EM
n  н CYRILLIC SMALL LETTER EN
o  о CYRILLIC SMALL LETTER O
p  п CYRILLIC SMALL LETTER PE
r  р CYRILLIC SMALL LETTER ER
s  с CYRILLIC SMALL LETTER ES
t  т CYRILLIC SMALL LETTER TE
u  у CYRILLIC SMALL LETTER U
f  ф CYRILLIC SMALL LETTER EF
h  х CYRILLIC SMALL LETTER HA
c  ц CYRILLIC SMALL LETTER TSE
ch ч CYRILLIC SMALL LETTER CHE
sh ш CYRILLIC SMALL LETTER SHA
sc щ CYRILLIC SMALL LETTER SHCHA
q  ъ CYRILLIC SMALL LETTER HARD SIGN
y  ы CYRILLIC SMALL LETTER YERU
w  ь CYRILLIC SMALL LETTER SOFT SIGN
je э CYRILLIC SMALL LETTER E
ye э CYRILLIC SMALL LETTER E
ju ю CYRILLIC SMALL LETTER YU
yu ю CYRILLIC SMALL LETTER YU
ja я CYRILLIC SMALL LETTER YA
ya я CYRILLIC SMALL LETTER YA

x ±
X №

" Modeline {{{
" ==============================================================================
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ==============================================================================
" }}} Modeline
