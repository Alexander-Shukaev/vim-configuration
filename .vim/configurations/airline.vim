" Preamble {{{
" ==============================================================================
"        File: airline.vim
" ------------------------------------------------------------------------------
"     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
" Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"     License: This program is free software: you can redistribute it and/or
"              modify it under the terms of the GNU General Public License as
"              published by the Free Software Foundation, either version 3 of
"              the License, or (at your option) any later version.
"
"              This program is distributed in the hope that it will be useful,
"              but WITHOUT ANY WARRANTY; without even the implied warranty of
"              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
"              General Public License for more details.
"
"              You should have received a copy of the GNU General Public License
"              along with this program. If not, see
"              <http://www.gnu.org/licenses/>.
" ==============================================================================
" }}} Preamble

" Functions {{{
" ==============================================================================
" Private {{{
" ==============================================================================
function! s:Airline()
  if exists('*airline#section#create')
    let g:airline_section_y =
    \ airline#section#create(
    \   [
    \     'bom',
    \     '%{&fileencoding}',
    \     ' ',
    \     '%{&fileformat}',
    \   ]
    \ )

    let g:airline_section_z =
    \ airline#section#create(['column_number', ' ', 'line_number', ' ', '%p%%'])

    let g:airline_section_warning = 0
  endif
endfunction
" ==============================================================================
" }}} Private
" ==============================================================================
" }}} Functions

let g:airline#extensions#bufferline#enabled             = 0
let g:airline#extensions#bufferline#overwrite_variables = 0

if has('gui_running')
  let g:airline_theme           = 'powerlineish'
  let g:airline_powerline_fonts = 1
else
  let g:airline_powerline_fonts = 0
endif

let
\ g:airline_mode_map = {
\   '__' : '-',
\   'n'  : 'N',
\   'i'  : 'I',
\   'R'  : 'R',
\   'c'  : 'C',
\   'v'  : 'V',
\   'V'  : 'V',
\   '' : 'V',
\   's'  : 'S',
\   'S'  : 'S',
\   '' : 'S',
\ }

runtime! autoload/airline/parts.vim

if exists('*airline#parts#define_raw')
  call
  \ airline#parts#define_raw(
  \   'bom',
  \   '%#__accent_red#'                                                        .
  \   '%'                                                                      .
  \   '{'                                                                      .
  \     '&bomb'                                                                .
  \     '?'                                                                    .
  \     My#Quote('BOM')                                                        .
  \     '.'                                                                    .
  \     My#Quote(' ')                                                          .
  \     ':'                                                                    .
  \     My#Quote('')                                                           .
  \   '}'                                                                      .
  \   '%#__restore__#'
  \ )

  call
  \ airline#parts#define_raw(
  \   'line_number',
  \   '%#__accent_bold#'                                                       .
  \   '%l'                                                                     .
  \   '%#__restore__#'                                                         .
  \   '/'                                                                      .
  \   '%L'
  \ )

  call
  \ airline#parts#define_raw(
  \   'column_number',
  \   '%#__accent_bold#'                                                       .
  \   '%v'                                                                     .
  \   '%#__restore__#'                                                         .
  \   '/'                                                                      .
  \   '%{&textwidth}'
  \ )
endif

autocmd VimEnter * call <SID>Airline()

" Modeline {{{
" ==============================================================================
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ==============================================================================
" }}} Modeline
