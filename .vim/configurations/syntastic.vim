" Preamble {{{
" ==============================================================================
"        File: syntastic.vim
" ------------------------------------------------------------------------------
"     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
" Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"     License: This program is free software: you can redistribute it and/or
"              modify it under the terms of the GNU General Public License as
"              published by the Free Software Foundation, either version 3 of
"              the License, or (at your option) any later version.
"
"              This program is distributed in the hope that it will be useful,
"              but WITHOUT ANY WARRANTY; without even the implied warranty of
"              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
"              General Public License for more details.
"
"              You should have received a copy of the GNU General Public License
"              along with this program. If not, see
"              <http://www.gnu.org/licenses/>.
" ==============================================================================
" }}} Preamble

let g:syntastic_check_on_open      = 1
let g:syntastic_echo_current_error = 1
let g:syntastic_enable_signs       = 1
let g:syntastic_auto_jump          = 0
let g:syntastic_auto_loc_list      = 2
let g:syntastic_loc_list_height    = 5

let
\ g:syntastic_mode_map = {
\   'mode'              : 'active',
\   'active_filetypes'  : [],
\   'passive_filetypes' : ['python']
\ }

" Modeline {{{
" ==============================================================================
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ==============================================================================
" }}} Modeline
