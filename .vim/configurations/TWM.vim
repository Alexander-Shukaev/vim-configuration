" Preamble {{{
" ==============================================================================
"        File: TWM.vim
" ------------------------------------------------------------------------------
"     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
" Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"     License: This program is free software: you can redistribute it and/or
"              modify it under the terms of the GNU General Public License as
"              published by the Free Software Foundation, either version 3 of
"              the License, or (at your option) any later version.
"
"              This program is distributed in the hope that it will be useful,
"              but WITHOUT ANY WARRANTY; without even the implied warranty of
"              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
"              General Public License for more details.
"
"              You should have received a copy of the GNU General Public License
"              along with this program. If not, see
"              <http://www.gnu.org/licenses/>.
" ==============================================================================
" }}} Preamble

if !&diff
  runtime! autoload/TWM.vim

  if exists('*TWM#New')
    autocmd VimEnter * call TWM#New()
  endif

  if exists('*TWM#RotateCW')
    autocmd VimEnter * call TWM#RotateCW()
  endif

  " NOTE: Although it is possible to make these calls direct, it is recommended
  " to postpone them until the `VimEnter` event. Otherwise, for example,
  " `airline` is not loaded into the second buffer (resulted from split).
  " Furthermore, one can experience visually irritating effects when Vim
  " performs `redraw` during "vimrc" loading.
endif

runtime! autoload/TWM/Mappings.vim

if exists('*TWM#Mappings#Map')
  call TWM#Mappings#Map()
endif

" runtime! autoload/TWM.vim

" if exists('*TWM#Focus')
" autocmd BufWinEnter * call TWM#Focus()
" autocmd BufWinLeave * call TWM#Focus()
" endif

nmap <A-w> <Plug>TWM#RotateCW
nmap <A-s> <Plug>TWM#RotateCCW

nmap <A-W> <Plug>TWM#RotateCW
nmap <A-S> <Plug>TWM#RotateCCW

nmap <A-d> <Plug>TWM#MasterWindow#Grow
nmap <A-a> <Plug>TWM#MasterWindow#Shrink

nmap <A-D> <Plug>TWM#MasterWindow#Maximize
nmap <A-A> <Plug>TWM#MasterWindow#Minimize

" Modeline {{{
" ==============================================================================
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ==============================================================================
" }}} Modeline
