" Preamble {{{
" ==============================================================================
"        File: unite.vim
" ------------------------------------------------------------------------------
"     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
" Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"     License: This program is free software: you can redistribute it and/or
"              modify it under the terms of the GNU General Public License as
"              published by the Free Software Foundation, either version 3 of
"              the License, or (at your option) any later version.
"
"              This program is distributed in the hope that it will be useful,
"              but WITHOUT ANY WARRANTY; without even the implied warranty of
"              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
"              General Public License for more details.
"
"              You should have received a copy of the GNU General Public License
"              along with this program. If not, see
"              <http://www.gnu.org/licenses/>.
" ==============================================================================
" }}} Preamble

" Functions {{{
" ==============================================================================
" Private {{{
" ==============================================================================
function! s:CreateUniteDir()
  call My#CreateDir(g:unite_data_directory, 0700)
endfunction
" ==============================================================================
" }}} Private
" ==============================================================================
" }}} Functions

call unite#filters#matcher_default#use(['matcher_fuzzy'])
" call unite#filters#sorter_default#use(['sorter_rank'])
call unite#set_profile('files', 'smartcase', 1)
call unite#custom#source('line,outline', 'matchers', 'matcher_fuzzy')
call unite#custom#source('file_rec', 'max_candidates', 0)

let g:unite_data_directory = expand('~/.cache/unite')

call s:CreateUniteDir()

let g:unite_enable_start_insert        = 0
let g:unite_source_history_yank_enable = 1

let g:unite_prompt = '>>> '

nnoremap <silent> <Leader>uf
\ :<C-u>Unite
\   -buffer-name=files
\   -auto-resize
\   -toggle
\   file_rec<CR>

nnoremap <silent> <Leader>uy
\ :<C-u>Unite
\   -buffer-name=yanks
\   history/yank<CR>

nnoremap <silent> <Leader>ul
\ :<C-u>Unite
\   -buffer-name=lines
\   -auto-resize
\   line<CR>

nnoremap <silent> <Leader>ub
\ :<C-u>Unite
\   -buffer-name=buffers
\   -auto-resize
\   buffer<CR>

nnoremap <silent> <Leader>u/
\ :<C-u>Unite
\   -buffer-name=search
\   -start-insert
\   -no-quit
\   grep:.<CR>

nnoremap <silent> <Leader>um
\ :<C-u>Unite
\   -buffer-name=mappings
\   -auto-resize
\   mapping<CR>

nnoremap <silent> <Leader>us
\ :<C-u>Unite
\   -quick-match
\   buffer<CR>

" Modeline {{{
" ==============================================================================
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ==============================================================================
" }}} Modeline
