" Preamble {{{
" ==============================================================================
"        File: My.vim
" ------------------------------------------------------------------------------
"     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
" Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"     License: This program is free software: you can redistribute it and/or
"              modify it under the terms of the GNU General Public License as
"              published by the Free Software Foundation, either version 3 of
"              the License, or (at your option) any later version.
"
"              This program is distributed in the hope that it will be useful,
"              but WITHOUT ANY WARRANTY; without even the implied warranty of
"              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
"              General Public License for more details.
"
"              You should have received a copy of the GNU General Public License
"              along with this program. If not, see
"              <http://www.gnu.org/licenses/>.
" ==============================================================================
" }}} Preamble

" Functions {{{
" ==============================================================================
" Public {{{
" ==============================================================================
function! My#Quote(string)
  return "'" . a:string . "'"
endfunction

function! My#CreateDir(path, permissions)
  let l:path = simplify(expand(a:path, 1))

  if !isdirectory(l:path)
    if exists('*mkdir')
      call mkdir(l:path, 'p', a:permissions)
    endif
  endif
endfunction

function! My#GetUserName()
  return expand('$USERNAME', 1)
endfunction

function! My#GetHostName()
  return hostname()
endfunction

function! My#GetCurrentWorkingDirPath()
  return fnamemodify(getcwd(), ':p:h:~')
endfunction

function! My#GetCurrentFilePath()
  return simplify(expand('%:p:.', 1))
endfunction

function! My#GetTitle()
  return
  \ '<'                                                                        .
  \ '%{My#GetUserName()}'                                                      .
  \ '@'                                                                        .
  \ '%{My#GetHostName()}'                                                      .
  \ '>'                                                                        .
  \ ' '                                                                        .
  \ '"'                                                                        .
  \ '%{My#GetCurrentWorkingDirPath()}'                                         .
  \ '"'                                                                        .
  \ '%'                                                                        .
  \ '{'                                                                        .
  \   'empty(My#GetCurrentFilePath())'                                         .
  \   '?'                                                                      .
  \   My#Quote('')                                                             .
  \   ':'                                                                      .
  \   My#Quote(' ')                                                            .
  \   '.'                                                                      .
  \   My#Quote('"')                                                            .
  \   '.'                                                                      .
  \   'My#GetCurrentFilePath()'                                                .
  \   '.'                                                                      .
  \   My#Quote('"')                                                            .
  \ '}'
endfunction
" ==============================================================================
" }}} Public
" ==============================================================================
" }}} Functions

" Modeline {{{
" ==============================================================================
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ==============================================================================
" }}} Modeline
