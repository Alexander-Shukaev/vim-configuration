" Preamble {{{
" ==============================================================================
"        File: cpp.vim
" ------------------------------------------------------------------------------
"     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
" Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"     License: This program is free software: you can redistribute it and/or
"              modify it under the terms of the GNU General Public License as
"              published by the Free Software Foundation, either version 3 of
"              the License, or (at your option) any later version.
"
"              This program is distributed in the hope that it will be useful,
"              but WITHOUT ANY WARRANTY; without even the implied warranty of
"              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
"              General Public License for more details.
"
"              You should have received a copy of the GNU General Public License
"              along with this program. If not, see
"              <http://www.gnu.org/licenses/>.
" ==============================================================================
" }}} Preamble

syntax match cpp_Function
\ /\zs\w\+\ze\s*\(<.*>\s*\)\=(/
\ contains=cCppParen

highlight default link cpp_Function Function

syntax match cpp_ScopeResolutionOperator
\ /::/

syntax match cpp_ScopeIdentifier
\ /\w\+\s*::/
\ contains=cpp_ScopeResolutionOperator

highlight default link cpp_ScopeIdentifier Identifier

" C++03 {{{
" ==============================================================================
syntax keyword cpp_std_Constant badbit
syntax keyword cpp_std_Constant digits
syntax keyword cpp_std_Constant digits10
syntax keyword cpp_std_Constant eofbit
syntax keyword cpp_std_Constant failbit
syntax keyword cpp_std_Constant goodbit
syntax keyword cpp_std_Constant has_denorm
syntax keyword cpp_std_Constant has_denorm_loss
syntax keyword cpp_std_Constant has_infinity
syntax keyword cpp_std_Constant has_quiet_NaN
syntax keyword cpp_std_Constant has_signaling_NaN
syntax keyword cpp_std_Constant is_bounded
syntax keyword cpp_std_Constant is_exact
syntax keyword cpp_std_Constant is_iec559
syntax keyword cpp_std_Constant is_integer
syntax keyword cpp_std_Constant is_modulo
syntax keyword cpp_std_Constant is_signed
syntax keyword cpp_std_Constant is_specialized
syntax keyword cpp_std_Constant max_digits10
syntax keyword cpp_std_Constant max_exponent
syntax keyword cpp_std_Constant max_exponent10
syntax keyword cpp_std_Constant min_exponent
syntax keyword cpp_std_Constant min_exponent10
syntax keyword cpp_std_Constant nothrow
syntax keyword cpp_std_Constant npos
syntax keyword cpp_std_Constant radix
syntax keyword cpp_std_Constant round_style
syntax keyword cpp_std_Constant tinyness_before
syntax keyword cpp_std_Constant traps
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Exception bad_alloc
syntax keyword cpp_std_Exception bad_array_new_length
syntax keyword cpp_std_Exception bad_exception
syntax keyword cpp_std_Exception bad_typeid bad_cast
syntax keyword cpp_std_Exception domain_error
syntax keyword cpp_std_Exception exception
syntax keyword cpp_std_Exception invalid_argument
syntax keyword cpp_std_Exception length_error
syntax keyword cpp_std_Exception logic_error
syntax keyword cpp_std_Exception out_of_range
syntax keyword cpp_std_Exception overflow_error
syntax keyword cpp_std_Exception range_error
syntax keyword cpp_std_Exception runtime_error
syntax keyword cpp_std_Exception underflow_error
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Function abort
syntax keyword cpp_std_Function abs
syntax keyword cpp_std_Function accumulate
syntax keyword cpp_std_Function acos
syntax keyword cpp_std_Function adjacent_difference
syntax keyword cpp_std_Function adjacent_find
syntax keyword cpp_std_Function adjacent_find_if
syntax keyword cpp_std_Function advance
syntax keyword cpp_std_Function any
syntax keyword cpp_std_Function append
syntax keyword cpp_std_Function arg
syntax keyword cpp_std_Function asctime
syntax keyword cpp_std_Function asin
syntax keyword cpp_std_Function assert
syntax keyword cpp_std_Function assign
syntax keyword cpp_std_Function at
syntax keyword cpp_std_Function atan
syntax keyword cpp_std_Function atan2
syntax keyword cpp_std_Function atexit
syntax keyword cpp_std_Function atof
syntax keyword cpp_std_Function atoi
syntax keyword cpp_std_Function atol
syntax keyword cpp_std_Function back
syntax keyword cpp_std_Function back_inserter
syntax keyword cpp_std_Function bad
syntax keyword cpp_std_Function beg
syntax keyword cpp_std_Function begin
syntax keyword cpp_std_Function binary_compose
syntax keyword cpp_std_Function binary_negate
syntax keyword cpp_std_Function binary_search
syntax keyword cpp_std_Function bind1st
syntax keyword cpp_std_Function bind2nd
syntax keyword cpp_std_Function binder1st
syntax keyword cpp_std_Function binder2nd
syntax keyword cpp_std_Function boolalpha
syntax keyword cpp_std_Function bsearch
syntax keyword cpp_std_Function c_str
syntax keyword cpp_std_Function calloc
syntax keyword cpp_std_Function capacity
syntax keyword cpp_std_Function ceil
syntax keyword cpp_std_Function cerr
syntax keyword cpp_std_Function cin
syntax keyword cpp_std_Function clear
syntax keyword cpp_std_Function clearerr
syntax keyword cpp_std_Function clock
syntax keyword cpp_std_Function clog
syntax keyword cpp_std_Function close
syntax keyword cpp_std_Function compare
syntax keyword cpp_std_Function conj
syntax keyword cpp_std_Function construct
syntax keyword cpp_std_Function copy
syntax keyword cpp_std_Function copy_backward
syntax keyword cpp_std_Function cos
syntax keyword cpp_std_Function cosh
syntax keyword cpp_std_Function count
syntax keyword cpp_std_Function count_if
syntax keyword cpp_std_Function cout
syntax keyword cpp_std_Function ctime
syntax keyword cpp_std_Function data
syntax keyword cpp_std_Function dec
syntax keyword cpp_std_Function defaultfloat
syntax keyword cpp_std_Function denorm_min
syntax keyword cpp_std_Function destroy
syntax keyword cpp_std_Function difftime
syntax keyword cpp_std_Function distance
syntax keyword cpp_std_Function div
syntax keyword cpp_std_Function empty
syntax keyword cpp_std_Function end
syntax keyword cpp_std_Function endl
syntax keyword cpp_std_Function ends
syntax keyword cpp_std_Function eof
syntax keyword cpp_std_Function epsilon
syntax keyword cpp_std_Function equal
syntax keyword cpp_std_Function equal_range
syntax keyword cpp_std_Function erase
syntax keyword cpp_std_Function exit
syntax keyword cpp_std_Function exp
syntax keyword cpp_std_Function fabs
syntax keyword cpp_std_Function fail
syntax keyword cpp_std_Function failure
syntax keyword cpp_std_Function fclose
syntax keyword cpp_std_Function feof
syntax keyword cpp_std_Function ferror
syntax keyword cpp_std_Function fflush
syntax keyword cpp_std_Function fgetc
syntax keyword cpp_std_Function fgetpos
syntax keyword cpp_std_Function fgets
syntax keyword cpp_std_Function fill
syntax keyword cpp_std_Function fill_n
syntax keyword cpp_std_Function find
syntax keyword cpp_std_Function find_end
syntax keyword cpp_std_Function find_first_not_of
syntax keyword cpp_std_Function find_first_of
syntax keyword cpp_std_Function find_if
syntax keyword cpp_std_Function find_last_not_of
syntax keyword cpp_std_Function find_last_of
syntax keyword cpp_std_Function first
syntax keyword cpp_std_Function fixed
syntax keyword cpp_std_Function flags
syntax keyword cpp_std_Function flip
syntax keyword cpp_std_Function floor
syntax keyword cpp_std_Function flush
syntax keyword cpp_std_Function flush
syntax keyword cpp_std_Function fmod
syntax keyword cpp_std_Function fopen
syntax keyword cpp_std_Function for_each
syntax keyword cpp_std_Function fprintf
syntax keyword cpp_std_Function fputc
syntax keyword cpp_std_Function fputs
syntax keyword cpp_std_Function fread
syntax keyword cpp_std_Function free
syntax keyword cpp_std_Function freopen
syntax keyword cpp_std_Function frexp
syntax keyword cpp_std_Function front
syntax keyword cpp_std_Function fscanf
syntax keyword cpp_std_Function fseek
syntax keyword cpp_std_Function fsetpos
syntax keyword cpp_std_Function ftell
syntax keyword cpp_std_Function fwide
syntax keyword cpp_std_Function fwprintf
syntax keyword cpp_std_Function fwrite
syntax keyword cpp_std_Function fwscanf
syntax keyword cpp_std_Function gcount
syntax keyword cpp_std_Function generate
syntax keyword cpp_std_Function generate_n
syntax keyword cpp_std_Function get
syntax keyword cpp_std_Function get_allocator
syntax keyword cpp_std_Function get_money
syntax keyword cpp_std_Function get_temporary_buffer
syntax keyword cpp_std_Function get_time
syntax keyword cpp_std_Function getc
syntax keyword cpp_std_Function getchar
syntax keyword cpp_std_Function getenv
syntax keyword cpp_std_Function getline
syntax keyword cpp_std_Function gets
syntax keyword cpp_std_Function gmtime
syntax keyword cpp_std_Function good
syntax keyword cpp_std_Function hex
syntax keyword cpp_std_Function hexfloat
syntax keyword cpp_std_Function ignore
syntax keyword cpp_std_Function imag
syntax keyword cpp_std_Function in
syntax keyword cpp_std_Function includes
syntax keyword cpp_std_Function infinity
syntax keyword cpp_std_Function inner_product
syntax keyword cpp_std_Function inplace_merge
syntax keyword cpp_std_Function insert
syntax keyword cpp_std_Function inserter
syntax keyword cpp_std_Function internal
syntax keyword cpp_std_Function ios
syntax keyword cpp_std_Function ios_base
syntax keyword cpp_std_Function iostate
syntax keyword cpp_std_Function iota
syntax keyword cpp_std_Function is_heap
syntax keyword cpp_std_Function is_open
syntax keyword cpp_std_Function isalnum
syntax keyword cpp_std_Function isalpha
syntax keyword cpp_std_Function iscntrl
syntax keyword cpp_std_Function isdigit
syntax keyword cpp_std_Function isgraph
syntax keyword cpp_std_Function islower
syntax keyword cpp_std_Function isprint
syntax keyword cpp_std_Function ispunct
syntax keyword cpp_std_Function isspace
syntax keyword cpp_std_Function isupper
syntax keyword cpp_std_Function isxdigit
syntax keyword cpp_std_Function iter_swap
syntax keyword cpp_std_Function iterator_category
syntax keyword cpp_std_Function jmp_buf
syntax keyword cpp_std_Function key_comp
syntax keyword cpp_std_Function labs
syntax keyword cpp_std_Function ldexp
syntax keyword cpp_std_Function ldiv
syntax keyword cpp_std_Function length
syntax keyword cpp_std_Function lexicographical_compare
syntax keyword cpp_std_Function lexicographical_compare_3way
syntax keyword cpp_std_Function llabs
syntax keyword cpp_std_Function lldiv
syntax keyword cpp_std_Function localtime
syntax keyword cpp_std_Function log
syntax keyword cpp_std_Function log10
syntax keyword cpp_std_Function longjmp
syntax keyword cpp_std_Function lower_bound
syntax keyword cpp_std_Function make_heap
syntax keyword cpp_std_Function make_pair
syntax keyword cpp_std_Function malloc
syntax keyword cpp_std_Function max
syntax keyword cpp_std_Function max_element
syntax keyword cpp_std_Function max_size
syntax keyword cpp_std_Function mem_fun
syntax keyword cpp_std_Function mem_fun_ref
syntax keyword cpp_std_Function memchr
syntax keyword cpp_std_Function memcpy
syntax keyword cpp_std_Function memmove
syntax keyword cpp_std_Function memset
syntax keyword cpp_std_Function merge
syntax keyword cpp_std_Function min
syntax keyword cpp_std_Function min_element
syntax keyword cpp_std_Function mismatch
syntax keyword cpp_std_Function mktime
syntax keyword cpp_std_Function modf
syntax keyword cpp_std_Function next_permutation
syntax keyword cpp_std_Function noboolalpha
syntax keyword cpp_std_Function none
syntax keyword cpp_std_Function norm
syntax keyword cpp_std_Function noshowbase
syntax keyword cpp_std_Function noshowpoint
syntax keyword cpp_std_Function noshowpos
syntax keyword cpp_std_Function noskipws
syntax keyword cpp_std_Function not1
syntax keyword cpp_std_Function not2
syntax keyword cpp_std_Function nounitbuf
syntax keyword cpp_std_Function nouppercase
syntax keyword cpp_std_Function nth_element
syntax keyword cpp_std_Function numeric_limits
syntax keyword cpp_std_Function oct
syntax keyword cpp_std_Function open
syntax keyword cpp_std_Function partial_sort
syntax keyword cpp_std_Function partial_sort_copy
syntax keyword cpp_std_Function partial_sum
syntax keyword cpp_std_Function partition
syntax keyword cpp_std_Function peek
syntax keyword cpp_std_Function perror
syntax keyword cpp_std_Function polar
syntax keyword cpp_std_Function pop
syntax keyword cpp_std_Function pop_back
syntax keyword cpp_std_Function pop_front
syntax keyword cpp_std_Function pop_heap
syntax keyword cpp_std_Function pow
syntax keyword cpp_std_Function power
syntax keyword cpp_std_Function precision
syntax keyword cpp_std_Function prev_permutation
syntax keyword cpp_std_Function printf
syntax keyword cpp_std_Function ptr_fun
syntax keyword cpp_std_Function push
syntax keyword cpp_std_Function push_back
syntax keyword cpp_std_Function push_front
syntax keyword cpp_std_Function push_heap
syntax keyword cpp_std_Function put
syntax keyword cpp_std_Function put_money
syntax keyword cpp_std_Function put_time
syntax keyword cpp_std_Function putback
syntax keyword cpp_std_Function putc
syntax keyword cpp_std_Function putchar
syntax keyword cpp_std_Function puts
syntax keyword cpp_std_Function qsort
syntax keyword cpp_std_Function quiet_NaN
syntax keyword cpp_std_Function raise
syntax keyword cpp_std_Function rand
syntax keyword cpp_std_Function random_sample
syntax keyword cpp_std_Function random_sample_n
syntax keyword cpp_std_Function random_shuffle
syntax keyword cpp_std_Function rbegin
syntax keyword cpp_std_Function rdbuf
syntax keyword cpp_std_Function rdstate
syntax keyword cpp_std_Function read
syntax keyword cpp_std_Function real
syntax keyword cpp_std_Function realloc
syntax keyword cpp_std_Function remove
syntax keyword cpp_std_Function remove_copy
syntax keyword cpp_std_Function remove_copy_if
syntax keyword cpp_std_Function remove_if
syntax keyword cpp_std_Function rename
syntax keyword cpp_std_Function rend
syntax keyword cpp_std_Function replace
syntax keyword cpp_std_Function replace_copy
syntax keyword cpp_std_Function replace_copy_if
syntax keyword cpp_std_Function replace_if
syntax keyword cpp_std_Function reserve
syntax keyword cpp_std_Function reset
syntax keyword cpp_std_Function resetiosflags
syntax keyword cpp_std_Function resize
syntax keyword cpp_std_Function return_temporary_buffer
syntax keyword cpp_std_Function reverse
syntax keyword cpp_std_Function reverse_copy
syntax keyword cpp_std_Function rewind
syntax keyword cpp_std_Function rfind
syntax keyword cpp_std_Function rotate
syntax keyword cpp_std_Function rotate_copy
syntax keyword cpp_std_Function round_error
syntax keyword cpp_std_Function scanf
syntax keyword cpp_std_Function scientific
syntax keyword cpp_std_Function search
syntax keyword cpp_std_Function search_n
syntax keyword cpp_std_Function second
syntax keyword cpp_std_Function seekg
syntax keyword cpp_std_Function seekp
syntax keyword cpp_std_Function set_difference
syntax keyword cpp_std_Function set_intersection
syntax keyword cpp_std_Function set_new_handler
syntax keyword cpp_std_Function set_symmetric_difference
syntax keyword cpp_std_Function set_union
syntax keyword cpp_std_Function setbase
syntax keyword cpp_std_Function setbuf
syntax keyword cpp_std_Function setf
syntax keyword cpp_std_Function setfill
syntax keyword cpp_std_Function setiosflags
syntax keyword cpp_std_Function setjmp
syntax keyword cpp_std_Function setlocale
syntax keyword cpp_std_Function setprecision
syntax keyword cpp_std_Function setvbuf
syntax keyword cpp_std_Function setw
syntax keyword cpp_std_Function showbase
syntax keyword cpp_std_Function showpoint
syntax keyword cpp_std_Function showpos
syntax keyword cpp_std_Function signal
syntax keyword cpp_std_Function signaling_NaN
syntax keyword cpp_std_Function sin
syntax keyword cpp_std_Function sinh
syntax keyword cpp_std_Function size
syntax keyword cpp_std_Function skipws
syntax keyword cpp_std_Function sort
syntax keyword cpp_std_Function sort_heap
syntax keyword cpp_std_Function splice
syntax keyword cpp_std_Function sprintf
syntax keyword cpp_std_Function sqrt
syntax keyword cpp_std_Function srand
syntax keyword cpp_std_Function sscanf
syntax keyword cpp_std_Function stable_partition
syntax keyword cpp_std_Function stable_sort
syntax keyword cpp_std_Function str
syntax keyword cpp_std_Function strcat
syntax keyword cpp_std_Function strchr
syntax keyword cpp_std_Function strcmp
syntax keyword cpp_std_Function strcoll
syntax keyword cpp_std_Function strcpy
syntax keyword cpp_std_Function strcspn
syntax keyword cpp_std_Function strerror
syntax keyword cpp_std_Function strftime
syntax keyword cpp_std_Function string
syntax keyword cpp_std_Function strlen
syntax keyword cpp_std_Function strncat
syntax keyword cpp_std_Function strncmp
syntax keyword cpp_std_Function strncpy
syntax keyword cpp_std_Function strpbrk
syntax keyword cpp_std_Function strrchr
syntax keyword cpp_std_Function strspn
syntax keyword cpp_std_Function strstr
syntax keyword cpp_std_Function strtod
syntax keyword cpp_std_Function strtok
syntax keyword cpp_std_Function strtol
syntax keyword cpp_std_Function strtoul
syntax keyword cpp_std_Function strxfrm
syntax keyword cpp_std_Function substr
syntax keyword cpp_std_Function swap
syntax keyword cpp_std_Function swap_ranges
syntax keyword cpp_std_Function swprintf
syntax keyword cpp_std_Function swscanf
syntax keyword cpp_std_Function sync_with_stdio
syntax keyword cpp_std_Function system
syntax keyword cpp_std_Function tan
syntax keyword cpp_std_Function tanh
syntax keyword cpp_std_Function tellg
syntax keyword cpp_std_Function tellp
syntax keyword cpp_std_Function test
syntax keyword cpp_std_Function time
syntax keyword cpp_std_Function time_t
syntax keyword cpp_std_Function tmpfile
syntax keyword cpp_std_Function tmpnam
syntax keyword cpp_std_Function to_string
syntax keyword cpp_std_Function to_ulong
syntax keyword cpp_std_Function to_wstring
syntax keyword cpp_std_Function tolower
syntax keyword cpp_std_Function top
syntax keyword cpp_std_Function toupper
syntax keyword cpp_std_Function transform
syntax keyword cpp_std_Function unary_compose
syntax keyword cpp_std_Function unget
syntax keyword cpp_std_Function ungetc
syntax keyword cpp_std_Function uninitialized_copy
syntax keyword cpp_std_Function uninitialized_copy_n
syntax keyword cpp_std_Function uninitialized_fill
syntax keyword cpp_std_Function uninitialized_fill_n
syntax keyword cpp_std_Function unique
syntax keyword cpp_std_Function unique_copy
syntax keyword cpp_std_Function unitbuf
syntax keyword cpp_std_Function unsetf
syntax keyword cpp_std_Function upper_bound
syntax keyword cpp_std_Function uppercase
syntax keyword cpp_std_Function va_arg
syntax keyword cpp_std_Function va_arg
syntax keyword cpp_std_Function va_copy
syntax keyword cpp_std_Function va_end
syntax keyword cpp_std_Function va_start
syntax keyword cpp_std_Function value_comp
syntax keyword cpp_std_Function vfprintf
syntax keyword cpp_std_Function vfwprintf
syntax keyword cpp_std_Function vprintf
syntax keyword cpp_std_Function vsprintf
syntax keyword cpp_std_Function vswprintf
syntax keyword cpp_std_Function vwprintf
syntax keyword cpp_std_Function width
syntax keyword cpp_std_Function wprintf
syntax keyword cpp_std_Function write
syntax keyword cpp_std_Function ws
syntax keyword cpp_std_Function wscanf
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Functional binary_function
syntax keyword cpp_std_Functional binary_negate
syntax keyword cpp_std_Functional bit_and
syntax keyword cpp_std_Functional bit_not
syntax keyword cpp_std_Functional bit_or
syntax keyword cpp_std_Functional divides
syntax keyword cpp_std_Functional equal_to
syntax keyword cpp_std_Functional greater
syntax keyword cpp_std_Functional greater_equal
syntax keyword cpp_std_Functional less
syntax keyword cpp_std_Functional less_equal
syntax keyword cpp_std_Functional logical_and
syntax keyword cpp_std_Functional logical_not
syntax keyword cpp_std_Functional logical_or
syntax keyword cpp_std_Functional minus
syntax keyword cpp_std_Functional modulus
syntax keyword cpp_std_Functional multiplies
syntax keyword cpp_std_Functional negate
syntax keyword cpp_std_Functional not_equal_to
syntax keyword cpp_std_Functional plus
syntax keyword cpp_std_Functional unary_function
syntax keyword cpp_std_Functional unary_negate
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Iterator back_insert_iterator
syntax keyword cpp_std_Iterator bidirectional_iterator
syntax keyword cpp_std_Iterator const_iterator
syntax keyword cpp_std_Iterator const_reverse_iterator
syntax keyword cpp_std_Iterator forward_iterator
syntax keyword cpp_std_Iterator front_insert_iterator
syntax keyword cpp_std_Iterator input_iterator
syntax keyword cpp_std_Iterator insert_iterator
syntax keyword cpp_std_Iterator istream_iterator
syntax keyword cpp_std_Iterator istreambuf_iterator
syntax keyword cpp_std_Iterator iterator
syntax keyword cpp_std_Iterator ostream_iterator
syntax keyword cpp_std_Iterator output_iterator
syntax keyword cpp_std_Iterator random_access_iterator
syntax keyword cpp_std_Iterator raw_storage_iterator
syntax keyword cpp_std_Iterator reverse_bidirectional_iterator
syntax keyword cpp_std_Iterator reverse_iterator
" ------------------------------------------------------------------------------
syntax keyword cpp_std_IteratorTag bidirectional_iterator_tag
syntax keyword cpp_std_IteratorTag forward_iterator_tag
syntax keyword cpp_std_IteratorTag input_iterator_tag
syntax keyword cpp_std_IteratorTag output_iterator_tag
syntax keyword cpp_std_IteratorTag random_access_iterator_tag
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Namespace rel_ops
syntax keyword cpp_std_Namespace std
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Type allocator
syntax keyword cpp_std_Type auto_ptr
syntax keyword cpp_std_Type basic_fstream
syntax keyword cpp_std_Type basic_ifstream
syntax keyword cpp_std_Type basic_istringstream
syntax keyword cpp_std_Type basic_ofstream
syntax keyword cpp_std_Type basic_ostringstream
syntax keyword cpp_std_Type basic_string
syntax keyword cpp_std_Type basic_stringstream
syntax keyword cpp_std_Type binary_compose
syntax keyword cpp_std_Type binder1st
syntax keyword cpp_std_Type binder2nd
syntax keyword cpp_std_Type bitset
syntax keyword cpp_std_Type char_traits
syntax keyword cpp_std_Type char_type
syntax keyword cpp_std_Type const_mem_fun1_t
syntax keyword cpp_std_Type const_mem_fun_ref1_t
syntax keyword cpp_std_Type const_mem_fun_ref_t
syntax keyword cpp_std_Type const_mem_fun_t
syntax keyword cpp_std_Type const_pointer
syntax keyword cpp_std_Type const_reference
syntax keyword cpp_std_Type deque
syntax keyword cpp_std_Type difference_type
syntax keyword cpp_std_Type div_t
syntax keyword cpp_std_Type double_t
syntax keyword cpp_std_Type filebuf
syntax keyword cpp_std_Type first_type
syntax keyword cpp_std_Type float_denorm_style
syntax keyword cpp_std_Type float_round_style
syntax keyword cpp_std_Type float_t
syntax keyword cpp_std_Type fstream
syntax keyword cpp_std_Type gslice_array
syntax keyword cpp_std_Type ifstream
syntax keyword cpp_std_Type imaxdiv_t
syntax keyword cpp_std_Type indirect_array
syntax keyword cpp_std_Type int_type
syntax keyword cpp_std_Type istringstream
syntax keyword cpp_std_Type iterator_traits
syntax keyword cpp_std_Type key_compare
syntax keyword cpp_std_Type key_type
syntax keyword cpp_std_Type ldiv_t
syntax keyword cpp_std_Type list
syntax keyword cpp_std_Type lldiv_t
syntax keyword cpp_std_Type map
syntax keyword cpp_std_Type mapped_type
syntax keyword cpp_std_Type mask_array
syntax keyword cpp_std_Type mem_fun1_t
syntax keyword cpp_std_Type mem_fun_ref1_t
syntax keyword cpp_std_Type mem_fun_ref_t
syntax keyword cpp_std_Type mem_fun_t
syntax keyword cpp_std_Type multimap
syntax keyword cpp_std_Type multiset
syntax keyword cpp_std_Type nothrow_t
syntax keyword cpp_std_Type off_type
syntax keyword cpp_std_Type ofstream
syntax keyword cpp_std_Type ostream
syntax keyword cpp_std_Type ostringstream
syntax keyword cpp_std_Type pair
syntax keyword cpp_std_Type pointer
syntax keyword cpp_std_Type pointer_to_binary_function
syntax keyword cpp_std_Type pointer_to_unary_function
syntax keyword cpp_std_Type pos_type
syntax keyword cpp_std_Type priority_queue
syntax keyword cpp_std_Type queue
syntax keyword cpp_std_Type reference
syntax keyword cpp_std_Type second_type
syntax keyword cpp_std_Type sequence_buffer
syntax keyword cpp_std_Type set
syntax keyword cpp_std_Type sig_atomic_t
syntax keyword cpp_std_Type size_type
syntax keyword cpp_std_Type slice_array
syntax keyword cpp_std_Type stack
syntax keyword cpp_std_Type stream
syntax keyword cpp_std_Type string
syntax keyword cpp_std_Type stringbuf
syntax keyword cpp_std_Type stringstream
syntax keyword cpp_std_Type temporary_buffer
syntax keyword cpp_std_Type test_type
syntax keyword cpp_std_Type tm
syntax keyword cpp_std_Type traits_type
syntax keyword cpp_std_Type type_info
syntax keyword cpp_std_Type u16string
syntax keyword cpp_std_Type u32string
syntax keyword cpp_std_Type unary_compose
syntax keyword cpp_std_Type unary_negate
syntax keyword cpp_std_Type valarray
syntax keyword cpp_std_Type value_compare
syntax keyword cpp_std_Type value_type
syntax keyword cpp_std_Type vector
syntax keyword cpp_std_Type wfstream
syntax keyword cpp_std_Type wifstream
syntax keyword cpp_std_Type wistringstream
syntax keyword cpp_std_Type wofstream
syntax keyword cpp_std_Type wostringstream
syntax keyword cpp_std_Type wstring
syntax keyword cpp_std_Type wstringbuf
syntax keyword cpp_std_Type wstringstream
" ==============================================================================
" }}} C++03

" C++11 {{{
" ==============================================================================
syntax keyword cpp_std_Type max_align_t
syntax keyword cpp_std_Type nullptr_t
syntax keyword cpp_std_Type type_index

" `type_traits` {{{
" ==============================================================================
syntax keyword cpp_std_Type add_const
syntax keyword cpp_std_Type add_cv
syntax keyword cpp_std_Type add_lvalue_reference
syntax keyword cpp_std_Type add_pointer
syntax keyword cpp_std_Type add_rvalue_reference
syntax keyword cpp_std_Type add_volatile
syntax keyword cpp_std_Type aligned_storage
syntax keyword cpp_std_Type aligned_union
syntax keyword cpp_std_Type alignment_of
syntax keyword cpp_std_Type common_type
syntax keyword cpp_std_Type conditional
syntax keyword cpp_std_Type decay
syntax keyword cpp_std_Type enable_if
syntax keyword cpp_std_Type extent
syntax keyword cpp_std_Type false_type
syntax keyword cpp_std_Type integral_constant
syntax keyword cpp_std_Type is_abstract
syntax keyword cpp_std_Type is_arithmetic
syntax keyword cpp_std_Type is_array
syntax keyword cpp_std_Type is_assignable
syntax keyword cpp_std_Type is_base_of
syntax keyword cpp_std_Type is_class
syntax keyword cpp_std_Type is_compound
syntax keyword cpp_std_Type is_const
syntax keyword cpp_std_Type is_constructible
syntax keyword cpp_std_Type is_convertible
syntax keyword cpp_std_Type is_copy_assignable
syntax keyword cpp_std_Type is_copy_constructible
syntax keyword cpp_std_Type is_default_constructible
syntax keyword cpp_std_Type is_destructible
syntax keyword cpp_std_Type is_empty
syntax keyword cpp_std_Type is_enum
syntax keyword cpp_std_Type is_floating_point
syntax keyword cpp_std_Type is_function
syntax keyword cpp_std_Type is_fundamental
syntax keyword cpp_std_Type is_integral
syntax keyword cpp_std_Type is_literal_type
syntax keyword cpp_std_Type is_lvalue_reference
syntax keyword cpp_std_Type is_member_function_pointer
syntax keyword cpp_std_Type is_member_object_pointer
syntax keyword cpp_std_Type is_member_pointer
syntax keyword cpp_std_Type is_move_assignable
syntax keyword cpp_std_Type is_move_constructible
syntax keyword cpp_std_Type is_nothrow_assignable
syntax keyword cpp_std_Type is_nothrow_constructible
syntax keyword cpp_std_Type is_nothrow_copy_assignable
syntax keyword cpp_std_Type is_nothrow_copy_constructible
syntax keyword cpp_std_Type is_nothrow_default_constructible
syntax keyword cpp_std_Type is_nothrow_move_assignable
syntax keyword cpp_std_Type is_nothrow_move_constructible
syntax keyword cpp_std_Type is_object
syntax keyword cpp_std_Type is_pod
syntax keyword cpp_std_Type is_pointer
syntax keyword cpp_std_Type is_polymorphic
syntax keyword cpp_std_Type is_reference
syntax keyword cpp_std_Type is_rvalue_reference
syntax keyword cpp_std_Type is_same
syntax keyword cpp_std_Type is_scalar
syntax keyword cpp_std_Type is_signed
syntax keyword cpp_std_Type is_standard_layout
syntax keyword cpp_std_Type is_trivial
syntax keyword cpp_std_Type is_trivially_assignable
syntax keyword cpp_std_Type is_trivially_constructible
syntax keyword cpp_std_Type is_trivially_copy_assignable
syntax keyword cpp_std_Type is_trivially_copy_constructible
syntax keyword cpp_std_Type is_trivially_copyable
syntax keyword cpp_std_Type is_trivially_default_constructible
syntax keyword cpp_std_Type is_trivially_destructible
syntax keyword cpp_std_Type is_trivially_move_assignable
syntax keyword cpp_std_Type is_trivially_move_constructible
syntax keyword cpp_std_Type is_union
syntax keyword cpp_std_Type is_unsigned
syntax keyword cpp_std_Type is_void
syntax keyword cpp_std_Type is_volatile
syntax keyword cpp_std_Type make_signed
syntax keyword cpp_std_Type make_unsigned
syntax keyword cpp_std_Type rank
syntax keyword cpp_std_Type remove_all_extents
syntax keyword cpp_std_Type remove_const
syntax keyword cpp_std_Type remove_cv
syntax keyword cpp_std_Type remove_extent
syntax keyword cpp_std_Type remove_pointer
syntax keyword cpp_std_Type remove_reference
syntax keyword cpp_std_Type remove_volatile
syntax keyword cpp_std_Type result_of
syntax keyword cpp_std_Type true_type
syntax keyword cpp_std_Type underlying_type
" ==============================================================================
" }}} `type_traits`

" `memory` {{{
" ==============================================================================
syntax keyword cpp_std_Constant allocator_arg
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Exception bad_weak_ptr
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Function addressof
syntax keyword cpp_std_Function align
syntax keyword cpp_std_Function declare_no_pointers
syntax keyword cpp_std_Function declare_reachable
syntax keyword cpp_std_Function get_pointer_safety
syntax keyword cpp_std_Function undeclare_no_pointers
syntax keyword cpp_std_Function undeclare_reachable
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Type allocator_arg_t
syntax keyword cpp_std_Type allocator_traits
syntax keyword cpp_std_Type allocator_type
syntax keyword cpp_std_Type default_delete
syntax keyword cpp_std_Type enable_shared_from_this
syntax keyword cpp_std_Type owner_less
syntax keyword cpp_std_Type pointer_safety
syntax keyword cpp_std_Type pointer_traits
syntax keyword cpp_std_Type scoped_allocator_adaptor
syntax keyword cpp_std_Type shared_ptr
syntax keyword cpp_std_Type unique_ptr
syntax keyword cpp_std_Type uses_allocator
syntax keyword cpp_std_Type weak_ptr
" ==============================================================================
" }}} `memory`

" `function` {{{
" ==============================================================================
syntax keyword cpp_std_Constant _1 _2 _3 _4 _5 _6 _7 _8 _9
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Exception bad_function_call
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Function bind
syntax keyword cpp_std_Function mem_fn
syntax keyword cpp_std_Function ref cref
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Functional function
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Type is_bind_expression
syntax keyword cpp_std_Type is_placeholder
syntax keyword cpp_std_Type reference_wrapper
" ==============================================================================
" }}} `function`

" `bitset` {{{
" ==============================================================================
syntax keyword cpp_std_Function all
syntax keyword cpp_std_Function to_ullong
" ==============================================================================
" }}} `bitset`

" `iterator` {{{
" ==============================================================================
syntax keyword cpp_std_Function make_move_iterator
syntax keyword cpp_std_Function next prev
syntax keyword cpp_std_Iterator move_iterator
" ==============================================================================
" }}} `iterator`

" `exit` {{{
" ==============================================================================
syntax keyword cpp_std_Function _Exit
syntax keyword cpp_std_Function at_quick_exit
syntax keyword cpp_std_Function quick_exit
" ==============================================================================
" }}} `exit`

" `chrono` {{{
" ==============================================================================
syntax keyword cpp_std_Namespace chrono
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Type duration
syntax keyword cpp_std_Type high_resolution_clock
syntax keyword cpp_std_Type steady_clock
syntax keyword cpp_std_Type system_clock
syntax keyword cpp_std_Type time_point
" ==============================================================================
" }}} `chrono`

" `tuple` {{{
" ==============================================================================
syntax keyword cpp_std_Function forward_as_tuple
syntax keyword cpp_std_Function make_tuple
syntax keyword cpp_std_Function tie
syntax keyword cpp_std_Function tuple_cat
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Type tuple
syntax keyword cpp_std_Type tuple_element
syntax keyword cpp_std_Type tuple_size
" ==============================================================================
" }}} `tuple`

" `array` {{{
" ==============================================================================
syntax keyword cpp_std_Type array
" ==============================================================================
" }}} `array`

" `container` {{{
" ==============================================================================
syntax keyword cpp_std_Function cbegin
syntax keyword cpp_std_Function cend
syntax keyword cpp_std_Function crbegin
syntax keyword cpp_std_Function crend
syntax keyword cpp_std_Function emplace
syntax keyword cpp_std_Function emplace_back
syntax keyword cpp_std_Function emplace_front
syntax keyword cpp_std_Function emplace_hint
syntax keyword cpp_std_Function shrink_to_fit
" ==============================================================================
" }}} `container`

" `forward_list` {{{
" ==============================================================================
syntax keyword cpp_std_Function before_begin
syntax keyword cpp_std_Function cbefore_begin
syntax keyword cpp_std_Function emplace_after
syntax keyword cpp_std_Function erase_after
syntax keyword cpp_std_Function insert_after
syntax keyword cpp_std_Function splice_after
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Type forward_list
" ==============================================================================
" }}} `forward_list`

" `unordered` {{{
" ==============================================================================
syntax keyword cpp_std_Function bucket
syntax keyword cpp_std_Function bucket_count
syntax keyword cpp_std_Function bucket_size
syntax keyword cpp_std_Function hash_function
syntax keyword cpp_std_Function key_eq
syntax keyword cpp_std_Function load_factor
syntax keyword cpp_std_Function max_bucket_count
syntax keyword cpp_std_Function max_load_factor
syntax keyword cpp_std_Function rehash
syntax keyword cpp_std_Function reserve
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Iterator const_local_iterator
syntax keyword cpp_std_Iterator local_iterator
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Type hash
syntax keyword cpp_std_Type hasher
syntax keyword cpp_std_Type key_equal
syntax keyword cpp_std_Type unordered_map
syntax keyword cpp_std_Type unordered_multimap
syntax keyword cpp_std_Type unordered_multiset
syntax keyword cpp_std_Type unordered_set
" ==============================================================================
" }}} `unordered`

" `algorithm` {{{
" ==============================================================================
syntax keyword cpp_std_Function all_of
syntax keyword cpp_std_Function any_of
syntax keyword cpp_std_Function copy_if
syntax keyword cpp_std_Function copy_n
syntax keyword cpp_std_Function find_if_not
syntax keyword cpp_std_Function is_heap_until
syntax keyword cpp_std_Function is_partitioned
syntax keyword cpp_std_Function is_permutation
syntax keyword cpp_std_Function is_sorted
syntax keyword cpp_std_Function is_sorted_until
syntax keyword cpp_std_Function itoa
syntax keyword cpp_std_Function minmax
syntax keyword cpp_std_Function minmax_element
syntax keyword cpp_std_Function move
syntax keyword cpp_std_Function move_backward
syntax keyword cpp_std_Function none_of
syntax keyword cpp_std_Function partition_copy
syntax keyword cpp_std_Function partition_point
syntax keyword cpp_std_Function shuffle
" ==============================================================================
" }}} `algorithm`

" `numerics` {{{
" ==============================================================================
syntax keyword cpp_std_Constant FLT_EVAL_METHOD
syntax keyword cpp_std_Constant FP_INFINITY
syntax keyword cpp_std_Constant FP_NAN
syntax keyword cpp_std_Constant FP_NORMAL
syntax keyword cpp_std_Constant FP_SUBNORMAL
syntax keyword cpp_std_Constant FP_ZERO
syntax keyword cpp_std_Constant HUGE_VALF
syntax keyword cpp_std_Constant HUGE_VALL
syntax keyword cpp_std_Constant INFINITY
syntax keyword cpp_std_Constant MATH_ERREXCEPT
syntax keyword cpp_std_Constant MATH_ERRNO
syntax keyword cpp_std_Constant NAN
syntax keyword cpp_std_Constant math_errhandling
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Function acosh
syntax keyword cpp_std_Function asinh
syntax keyword cpp_std_Function atanh
syntax keyword cpp_std_Function cbrt
syntax keyword cpp_std_Function copysign
syntax keyword cpp_std_Function erf
syntax keyword cpp_std_Function erfc
syntax keyword cpp_std_Function exp2
syntax keyword cpp_std_Function expm1
syntax keyword cpp_std_Function fdim
syntax keyword cpp_std_Function fma
syntax keyword cpp_std_Function fmax
syntax keyword cpp_std_Function fmin
syntax keyword cpp_std_Function fpclassify
syntax keyword cpp_std_Function hypot
syntax keyword cpp_std_Function ilogb
syntax keyword cpp_std_Function imaxabs
syntax keyword cpp_std_Function imaxdiv
syntax keyword cpp_std_Function isfinite
syntax keyword cpp_std_Function isinf
syntax keyword cpp_std_Function isnan
syntax keyword cpp_std_Function isnormal
syntax keyword cpp_std_Function lgamma
syntax keyword cpp_std_Function llrint
syntax keyword cpp_std_Function llround
syntax keyword cpp_std_Function log1p
syntax keyword cpp_std_Function log2
syntax keyword cpp_std_Function logb
syntax keyword cpp_std_Function lrint
syntax keyword cpp_std_Function lround
syntax keyword cpp_std_Function nan
syntax keyword cpp_std_Function nanf
syntax keyword cpp_std_Function nanl
syntax keyword cpp_std_Function nearbyint
syntax keyword cpp_std_Function nextafter
syntax keyword cpp_std_Function nexttoward
syntax keyword cpp_std_Function remainder
syntax keyword cpp_std_Function remquo
syntax keyword cpp_std_Function rint
syntax keyword cpp_std_Function round
syntax keyword cpp_std_Function scalbln
syntax keyword cpp_std_Function scalbn
syntax keyword cpp_std_Function signbit
syntax keyword cpp_std_Function tgamma
syntax keyword cpp_std_Function trunc
" ==============================================================================
" }}} `numerics`

" `complex` {{{
" ==============================================================================
syntax keyword cpp_std_Function proj
" ==============================================================================
" }}} `complex`

" `random` {{{
" ==============================================================================
syntax keyword cpp_std_Function generate_canonical
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Type bernoulli_distribution
syntax keyword cpp_std_Type binomial_distribution
syntax keyword cpp_std_Type cauchy_distribution
syntax keyword cpp_std_Type chi_squared_distribution
syntax keyword cpp_std_Type default_random_engine
syntax keyword cpp_std_Type discard_block_engine
syntax keyword cpp_std_Type discrete_distribution
syntax keyword cpp_std_Type exponential_distribution
syntax keyword cpp_std_Type extreme_value_distribution
syntax keyword cpp_std_Type fisher_f_distribution
syntax keyword cpp_std_Type gamma_distribution
syntax keyword cpp_std_Type geometric_distribution
syntax keyword cpp_std_Type independent_bits_engine
syntax keyword cpp_std_Type knuth_b
syntax keyword cpp_std_Type linear_congruential_engine
syntax keyword cpp_std_Type lognormal_distribution
syntax keyword cpp_std_Type mersenne_twister_engine
syntax keyword cpp_std_Type minstd_rand
syntax keyword cpp_std_Type minstd_rand0
syntax keyword cpp_std_Type mt19937
syntax keyword cpp_std_Type mt19937_64
syntax keyword cpp_std_Type negative_binomial_distribution
syntax keyword cpp_std_Type normal_distribution
syntax keyword cpp_std_Type piecewise_constant_distribution
syntax keyword cpp_std_Type piecewise_linear_distribution
syntax keyword cpp_std_Type poisson_distribution
syntax keyword cpp_std_Type random_device
syntax keyword cpp_std_Type ranlux24
syntax keyword cpp_std_Type ranlux24_base
syntax keyword cpp_std_Type ranlux48
syntax keyword cpp_std_Type ranlux48_base
syntax keyword cpp_std_Type seed_seq
syntax keyword cpp_std_Type shuffle_order_engine
syntax keyword cpp_std_Type student_t_distribution
syntax keyword cpp_std_Type subtract_with_carry_engine
syntax keyword cpp_std_Type uniform_int_distribution
syntax keyword cpp_std_Type uniform_real_distribution
syntax keyword cpp_std_Type weibull_distribution
" ==============================================================================
" }}} `random`

" `io` {{{
" ==============================================================================
syntax keyword cpp_std_Enum io_errc
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Function iostream_category
syntax keyword cpp_std_Function snprintf
syntax keyword cpp_std_Function vfscanf
syntax keyword cpp_std_Function vfwscanf
syntax keyword cpp_std_Function vscanf
syntax keyword cpp_std_Function vsnprintf
syntax keyword cpp_std_Function vsscanf
syntax keyword cpp_std_Function vswscanf
syntax keyword cpp_std_Function vwscanf
" ==============================================================================
" }}} `io`

" `locale` {{{
" ==============================================================================
syntax keyword cpp_std_Function isblank
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Type codecvt_mode
syntax keyword cpp_std_Type codecvt_utf16
syntax keyword cpp_std_Type codecvt_utf8
syntax keyword cpp_std_Type codecvt_utf8_utf16
syntax keyword cpp_std_Type wbuffer_convert
syntax keyword cpp_std_Type wstring_convert
" ==============================================================================
" }}} `locale`

" `regex` {{{
" ==============================================================================
syntax keyword cpp_std_Exception regex_error
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Iterator regex_iterator
syntax keyword cpp_std_Iterator regex_token_iterator
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Type basic_regex
syntax keyword cpp_std_Type error_type
syntax keyword cpp_std_Type match_flag_type
syntax keyword cpp_std_Type match_results
syntax keyword cpp_std_Type regex_match
syntax keyword cpp_std_Type regex_replace
syntax keyword cpp_std_Type regex_search
syntax keyword cpp_std_Type regex_traits
syntax keyword cpp_std_Type sub_match
syntax keyword cpp_std_Type syntax_option_type
" ==============================================================================
" }}} `regex`

" `atomic` {{{
" ==============================================================================
syntax keyword cpp_std_Constant ATOMIC_FLAG_INIT
syntax keyword cpp_std_Constant atomic_signal_fence
syntax keyword cpp_std_Constant atomic_thread_fence
syntax keyword cpp_std_Constant kill_dependency
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Function ATOMIC_VAR_INIT
syntax keyword cpp_std_Function atomic_compare_exchange_strong
syntax keyword cpp_std_Function atomic_compare_exchange_strong_explicit
syntax keyword cpp_std_Function atomic_compare_exchange_weak
syntax keyword cpp_std_Function atomic_compare_exchange_weak_explicit
syntax keyword cpp_std_Function atomic_exchange
syntax keyword cpp_std_Function atomic_exchange_explicit
syntax keyword cpp_std_Function atomic_fetch_add
syntax keyword cpp_std_Function atomic_fetch_add_explicit
syntax keyword cpp_std_Function atomic_fetch_and
syntax keyword cpp_std_Function atomic_fetch_and_explicit
syntax keyword cpp_std_Function atomic_fetch_or
syntax keyword cpp_std_Function atomic_fetch_or_explicit
syntax keyword cpp_std_Function atomic_fetch_sub
syntax keyword cpp_std_Function atomic_fetch_sub_explicit
syntax keyword cpp_std_Function atomic_fetch_xor
syntax keyword cpp_std_Function atomic_fetch_xor_explicit
syntax keyword cpp_std_Function atomic_flag_clear
syntax keyword cpp_std_Function atomic_flag_clear_explicit
syntax keyword cpp_std_Function atomic_flag_test_and_set
syntax keyword cpp_std_Function atomic_flag_test_and_set_explicit
syntax keyword cpp_std_Function atomic_init
syntax keyword cpp_std_Function atomic_is_lock_free
syntax keyword cpp_std_Function atomic_load
syntax keyword cpp_std_Function atomic_load_explicit
syntax keyword cpp_std_Function atomic_store
syntax keyword cpp_std_Function atomic_store_explicit
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Type atomic
syntax keyword cpp_std_Type atomic_flag
syntax keyword cpp_std_Type memory_order
" ==============================================================================
" }}} `atomic`

" `thread` {{{
" ==============================================================================
syntax keyword cpp_std_Constant adopt_lock
syntax keyword cpp_std_Constant defer_lock
syntax keyword cpp_std_Constant try_to_lock
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Enum cv_status
syntax keyword cpp_std_Enum future_errc
syntax keyword cpp_std_Enum future_status
syntax keyword cpp_std_Enum launch
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Function async
syntax keyword cpp_std_Function call_once
syntax keyword cpp_std_Function detach
syntax keyword cpp_std_Function future_category
syntax keyword cpp_std_Function get_id
syntax keyword cpp_std_Function get_id
syntax keyword cpp_std_Function hardware_concurrency
syntax keyword cpp_std_Function join
syntax keyword cpp_std_Function joinable
syntax keyword cpp_std_Function lock
syntax keyword cpp_std_Function native_handle
syntax keyword cpp_std_Function notify_all_at_thread_exit
syntax keyword cpp_std_Function sleep_for
syntax keyword cpp_std_Function sleep_until
syntax keyword cpp_std_Function try_lock
syntax keyword cpp_std_Function yield
" ------------------------------------------------------------------------------
syntax keyword cpp_std_Type adopt_lock_t
syntax keyword cpp_std_Type condition_variable
syntax keyword cpp_std_Type condition_variable_any
syntax keyword cpp_std_Type defer_lock_t
syntax keyword cpp_std_Type future
syntax keyword cpp_std_Type future_error
syntax keyword cpp_std_Type lock_guard
syntax keyword cpp_std_Type mutex
syntax keyword cpp_std_Type once_flag
syntax keyword cpp_std_Type packaged_task
syntax keyword cpp_std_Type promise
syntax keyword cpp_std_Type recursive_mutex
syntax keyword cpp_std_Type recursive_timed_mutex
syntax keyword cpp_std_Type shared_future
syntax keyword cpp_std_Type thread
syntax keyword cpp_std_Type timed_mutex
syntax keyword cpp_std_Type try_to_lock_t
syntax keyword cpp_std_Type unique_lock
" ==============================================================================
" }}} `thread`

" `string` {{{
" ==============================================================================
syntax keyword cpp_std_Function stod
syntax keyword cpp_std_Function stof
syntax keyword cpp_std_Function stoi
syntax keyword cpp_std_Function stol
syntax keyword cpp_std_Function stold
syntax keyword cpp_std_Function stoll
syntax keyword cpp_std_Function stoul
syntax keyword cpp_std_Function stoull
" ==============================================================================
" }}} `string`

" `ratio` {{{
" ==============================================================================
syntax keyword cpp_std_Type atto
syntax keyword cpp_std_Type centi
syntax keyword cpp_std_Type deca
syntax keyword cpp_std_Type deci
syntax keyword cpp_std_Type exa
syntax keyword cpp_std_Type femto
syntax keyword cpp_std_Type giga
syntax keyword cpp_std_Type hecto
syntax keyword cpp_std_Type kilo
syntax keyword cpp_std_Type mega
syntax keyword cpp_std_Type micro
syntax keyword cpp_std_Type milli
syntax keyword cpp_std_Type nano
syntax keyword cpp_std_Type peta
syntax keyword cpp_std_Type pico
syntax keyword cpp_std_Type ratio
syntax keyword cpp_std_Type ratio_add
syntax keyword cpp_std_Type ratio_divide
syntax keyword cpp_std_Type ratio_equal
syntax keyword cpp_std_Type ratio_greater
syntax keyword cpp_std_Type ratio_greater_equal
syntax keyword cpp_std_Type ratio_less
syntax keyword cpp_std_Type ratio_less_equal
syntax keyword cpp_std_Type ratio_multiply
syntax keyword cpp_std_Type ratio_not_equal
syntax keyword cpp_std_Type ratio_subtract
syntax keyword cpp_std_Type tera
syntax keyword cpp_std_Type yocto
syntax keyword cpp_std_Type yotta
syntax keyword cpp_std_Type zepto
syntax keyword cpp_std_Type zetta
" ==============================================================================
" }}} `ratio`

" `limits` {{{
" ==============================================================================
syntax keyword cpp_std_Function lowest
" ==============================================================================
" }}} `limits`
" ==============================================================================
" }}} C++11

highlight default link cpp_std_Constant    Constant
highlight default link cpp_std_Enum        Type
highlight default link cpp_std_Exception   Type
highlight default link cpp_std_Function    Function
highlight default link cpp_std_Functional  Type
highlight default link cpp_std_Iterator    Type
highlight default link cpp_std_IteratorTag Type
highlight default link cpp_std_Namespace   Constant
highlight default link cpp_std_Type        Type

" Modeline {{{
" ==============================================================================
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ==============================================================================
" }}} Modeline
