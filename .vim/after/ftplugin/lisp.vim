" Preamble {{{
" ==============================================================================
"        File: lisp.vim
" ------------------------------------------------------------------------------
"     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
" Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"     License: This program is free software: you can redistribute it and/or
"              modify it under the terms of the GNU General Public License as
"              published by the Free Software Foundation, either version 3 of
"              the License, or (at your option) any later version.
"
"              This program is distributed in the hope that it will be useful,
"              but WITHOUT ANY WARRANTY; without even the implied warranty of
"              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
"              General Public License for more details.
"
"              You should have received a copy of the GNU General Public License
"              along with this program. If not, see
"              <http://www.gnu.org/licenses/>.
" ==============================================================================
" }}} Preamble

" Indentation {{{
" ==============================================================================
" Number of spaces that a tab counts for when displaying a file.
let &l:tabstop = 2

" Number of spaces to use for each step of (automatic) indentation.
let &l:shiftwidth = 2

" Use spaces instead of tabs.
let &l:expandtab = 1
" ==============================================================================
" }}} Indentation

" Wrapping {{{
" ==============================================================================
" 1. Auto-wrap comments using `textwidth`;
" 2. Allow formatting of comments with `gq`;
" 3. Auto-insert comment leader after hitting `<CR>` in insert mode.
let
\ &l:formatoptions =
\ join(
\   [
\     'c',
\     'q',
\     'r',
\   ],
\   ','
\ )
" ==============================================================================
" }}} Wrapping

" Hooks {{{
" ==============================================================================
autocmd BufWritePre <buffer> call RemoveBlank()
" ==============================================================================
" }}} Hooks

" Modeline {{{
" ==============================================================================
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ==============================================================================
" }}} Modeline
